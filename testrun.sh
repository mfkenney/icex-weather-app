#!/bin/bash

PORT=8082

gunicorn -w 4 --threads 2 -k sync -b 0.0.0.0:$PORT -t 45 weather.app:app
