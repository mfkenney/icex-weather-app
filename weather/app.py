#!/usr/bin/env python
#
# Flask web-app for ICEX weather station.
#
from flask import Flask, Response, render_template, json, jsonify
import redis
import time
from collections import defaultdict
from .proxy import ReverseProxied

# Configuration
REDIS_HOST = 'localhost'
REDIS_PORT = 6379
REDIS_DB = 0


app = Flask(__name__)
app.wsgi_app = ReverseProxied(app.wsgi_app)
app.config.from_object(__name__)


def connect_redis():
    """
    :rtype: redis.StrictRedis
    """
    return redis.StrictRedis(host=app.config['REDIS_HOST'],
                             port=app.config['REDIS_PORT'],
                             db=app.config['REDIS_DB'])


def event_stream(rd):
    """
    Generator for Server-Sent Events.

    :param rd: Redis server interface.
    :type rd: redis.StrictRedis
    """
    pubsub = rd.pubsub()
    pubsub.subscribe('data.weather')
    for msg in pubsub.listen():
        if msg['type'] == 'message':
            yield 'event: weather\ndata: {0}\n\n'.format(msg['data'])


@app.route('/')
def index():
    return render_template('site.html')


@app.route("/stream")
def stream():
    rd = connect_redis()
    return Response(event_stream(rd), mimetype='text/event-stream')


@app.route("/history")
def history():
    rd = connect_redis()
    table = defaultdict(list)
    for rec in rd.lrange('weather.history', 0, -1):
        for k, v in json.loads(rec).items():
            table[k].append(v)
    return jsonify(**table)
