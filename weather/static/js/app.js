// Weather data display app

define(["jquery", "moment", "hchart", "plot"], function($, M, hc, plot) {
    // Define all variables available in the Met Station data record
    // along with some derived values.
    var dataTable = {
        awspd: {
            title: "Wind Speed",
            units: "kts",
            src: "awspd",
            converter: function(x) {
                return Number(x*3600./1852.).toFixed(1);
            }
        },
        awdr: {
            title: "Wind Direction",
            units: "°",
            min: 0,
            max: 360
        },
        gust: {
            title: "Wind Gust",
            units: "kts",
            min: 0,
            max: 50,
            src: "gust",
            converter: function(x) {
                return Number(x*3600./1852.).toFixed(1);
            }
        },
        temp: {
            title: "Air Temperature",
            units: "°C",
            min: -40,
            max: 0
        },
        ftemp: {
            title: "Air Temperature",
            units: "°F",
            src: "temp",
            converter: function(x) {
                return Number(32. + x*9./5.).toFixed(1);
            }
        },
        bp: {
            title: "Air Pressure",
            units: "mb",
            min: 990,
            max: 1040
        },
        rh: {
            title: "Humidity",
            units: "%",
            min: 0,
            max: 100
        },
        dp: {
            title: "Dew Point",
            units: "°C",
            generator: function(data) {
                // http://en.wikipedia.org/wiki/Dew_point#Calculating_the_dew_point
                var b = 17.966, c = 247.15;
                var dp = Math.log(data.rh/100.) + b*data.temp/(c+data.temp);
                return Number(dp).toFixed(1);
            }
        },
        wc: {
            title: "Wind Chill",
            units: "°C",
            generator: function(data) {
                // http://en.wikipedia.org/wiki/Wind_chill#North_American_wind_chill_index
                if(data.awspd > 1.33) {
                    var a = 13.12, b = 0.6215, c = -11.37, d = 0.3965;
                    var v_scaled = Math.pow(data.awspd*3.6, 0.16);
                    var wc = a + b*data.temp + c*v_scaled + d*data.temp*v_scaled;
                    return Number(wc).toFixed(1);
                }
                return data.temp;
            }
        },
        fwc: {
            title: "Wind Chill",
            units: "°F",
            generator: function(data) {
                // http://en.wikipedia.org/wiki/Wind_chill#North_American_wind_chill_index
                if(data.awspd > 1.33) {
                    var a = 13.12, b = 0.6215, c = -11.37, d = 0.3965;
                    var v_scaled = Math.pow(data.awspd*3.6, 0.16);
                    var wc = a + b*data.temp + c*v_scaled + d*data.temp*v_scaled;
                    return Number(32. + wc*9./5.).toFixed(1);
                }
                return Number(32. + data.temp*9./5.).toFixed(1);
            }
        },
        vbatt: {
            title: "Battery Voltage",
            units: "volts"
        }
    };

    // Time-stamp display format
    var tformat = "YYYY-MM-DD HH:mm:ss";

    // Emulate the Python zip function to merge the elements of
    // multiple arrays.
    // http://stackoverflow.com/questions/4856717/javascript-equivalent-of-pythons-zip-function
    function zip() {
        var args = [].slice.call(arguments);
        var shortest = args.length==0 ? [] : args.reduce(function(a,b){
            return a.length<b.length ? a : b;
        });

        return shortest.map(function(_,i){
            return args.map(function(array){return array[i];});
        });
    }

    function initialize(dataurl, streamurl, container, plots, np) {
        var $timestamp = $("#timestamp");
        var selectors = {};
        var last_data;

        var series = $.map(plots, function(obj, i) {
            obj["type"] = "spline";
            obj["data"] = [];
            return obj;
        });
        var names = $.map(plots, function(obj, i) {
            return obj["name"];
        });
        var chart = plot.initialize(dataTable, container, series);

        // Cache the jQuery selectors
        $.each(dataTable, function(name, obj) {
            selectors[name] = $("#"+name);
        });

        // Fetch the initial plot data
        chart.showLoading();
        $.getJSON(dataurl, function(data) {
            var t = $.map(data["timestamp"], function(e, i) {
                return e*1000;
            });
            $.each(names, function(idx, name) {
                var obj = dataTable[name];
                var x;
                if(obj.converter) {
                    x = $.map(data[obj.src], function(e, i) {
                        return parseFloat(obj.converter(e));
                    });
                } else {
                    x = data[name];
                }
                plot.load(chart, idx, zip(t, x));
            });
            chart.redraw();
        });

        var source = new EventSource(streamurl);
        source.addEventListener("weather", function(e) {
            var data = JSON.parse(e.data);
            chart.hideLoading();
            $timestamp.text(M(data.timestamp*1000).utc().format(tformat) + "Z");

            // Update the tabular display
            $.each(dataTable, function(name, obj) {
                var x;
                if (obj.generator) {
                    x = obj.generator(data);
                } else if(obj.converter) {
                    x = obj.converter(data[obj.src]);
                } else {
                    x = data[name];
                }
                selectors[name].text(""+x+obj.units);
            });

            // Update the plot
            $.each(names, function(idx, name) {
                var x;
                var obj = dataTable[name];
                var t = data["timestamp"]*1000;

                if(obj.generator) {
                    x = parseFloat(obj.generator(data));
                } else if(obj.converter) {
                    x = parseFloat(obj.converter(data[obj.src]));
                } else {
                    x = data[name];
                }
                plot.update(chart, idx, [t, x], np);
            });
            chart.redraw();
        });
    }

    return {
        initialize: initialize
    };
});
