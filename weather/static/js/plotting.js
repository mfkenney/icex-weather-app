// Module to manage the Met Station plot.
define(["jquery", "hchart"], function($, hc) {

    function plot_initialize(dtab, id, series) {
        var options = {
            chart: {
                renderTo: id,
                type: "spline"
            },
            title: {
                text: "Real-time Weather Data"
            },
            xAxis: {
                type: "datetime",
                title: {
                    text: null
                }
            },
            yAxis: [{
                labels: {
                    formatter: function() {
                        return this.value + dtab[series[0].name].units;
                    },
                    style: {
                        color: series[0].color
                    }
                },
                title: {
                    text: dtab[series[0].name].title,
                    style: {
                        color: series[0].color
                    }
                }
            },{
                gridLineWidth: 0,
                title: {
                    text: dtab[series[1].name].title,
                    style: {
                        color: series[1].color
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value + " " + dtab[series[1].name].units;
                    },
                    style: {
                        color: series[1].color
                    }
                },
                opposite: true
            },{
                gridLineWidth: 0,
                title: {
                    text: dtab[series[2].name].title,
                    style: {
                        color: series[2].color
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value + dtab[series[2].name].units;
                    },
                    style: {
                        color: series[2].color
                    }
                },
                opposite: true
                //min: 0,
                //max: 359
            }],
            tooltip: {
                formatter: function () {
                    var unit = dtab[this.series.name].units;
                    return ''+
                        Highcharts.dateFormat("%d %b %H:%M:%S", this.x) +
                        '<br/> '+ this.y + unit;
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    lineWidth: 1,
                    marker: {
                        enabled: false,
                        states: {
                            hover: {
                                enabled: true,
                                radius: 4
                            }
                        }
                    }
                }
            },
            series: series
        };

        Highcharts.setOptions({
            lang: {
                loading: "Waiting for data ..."
            }
        });
        return new Highcharts.Chart(options);
    }

    function plot_loaddata(chart, index, data) {
        chart.series[index].setData(data, false, false, false);
    }

    function plot_update(chart, index, data, np) {
        var series = chart.series[index];
        var shift = series.data.length > np;

        chart.series[index].addPoint(data, false, shift);
    }

    return {
        update: plot_update,
        load: plot_loaddata,
        initialize: plot_initialize
    };
});
