// Module loader
require.config({
    paths: {
        baseUrl: "static/js",
        hchart: [
            "lib/highcharts",
            "lib/highcharts-more",
        ],
        jquery: "lib/jquery.min",
        moment: "lib/moment.min",
        underscore: "lib/underscore-min",
        app: "./app",
        plot: "./plotting"
    },
    config: {
        moment: {
            noGlobal: true
        }
    }
});
